#!/bin/sh
# source shortcuts.sh
# terms filename.tex
# acronyms filename.tex

terms(){
    grep -o -P "(?:[A-Z][a-z]+)\s+(?:\s*[A-Z][a-zA-Z]+)+" $1 | sort | uniq -c | sort -nr
}

acronyms(){
    grep -o -P "\b(?:[A-Z][a-z]*){2,}\b" "$@" | cut -d ":" -f2 | sort | uniq -c | sort -nr
}

bibkeys(){
    used=-1
    help="find the used or unused bibkeys using the bibliography file and the bcf file
    
    bibkeys [-un] references.bib paper.bcf
    
    where:
        -u used bibkeys 
        -n unused bibkeys
        -h show this help message
    
    Usage:
        To get a list of used bibkeys
            bibkeys references.bib paper.bcf
            bibkeys -u references.bib paper.bcf
        To get a list of unused bibkeys
            bibkeys -n references.bib paper.bcf
    "
    while getopts 'unh' flag
    do
        case "${flag}" in
            h)  echo "$help"
                return
                ;;
            \?) echo "$help" 
                return 1
                ;;
            u)  used=1
                shift 1
                ;;
            n)  used=0
                shift 1
                ;;
        esac
    done
    if [ $# -lt 2 ]
    then
        echo "$help" 
        return 1
    fi
    if [ $used == -1 ]
    then
        used=1
    fi
    
    bib=$1
    bcf=$2
    
    keys=`cat $bib | grep -E "\@[a-zA-Z]+\{" | cut -d{ -f2 | cut -d, -f1`
    for k in $keys 
    do
        if [ `grep -c $k $bcf` -gt 0 ]
        then
            found=1
        else
            found=0
        fi
        
        if [ $found == $used ]
        then
            echo $k
        fi
    done
}
